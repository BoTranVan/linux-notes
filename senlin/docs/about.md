### 1. Giới thiệu về Senlin

### Nội dung
- [1.1 Giới thiệu về Senlin](#about)
- [1.2 Cài đặt và cấu hình](#install)
- [1.3 Kiểm tra](#result)

---

### [1.1 Giới thiệu về Senlin](#about)
- Senlin is a clustering service for OpenStack clouds. It creates and operates clusters of homogeneous objects exposed by other OpenStack services. The goal is to make orchestration of collections of similar objects easier.

- Senlin interacts with other OpenStack services so that clusters of resources exposed by those services can be created and operated. These interactions are mostly done through the via profile plugins. Each profile type implementation enable Senlin to create, update, delete a specific type of resources.

- A Cluster can be associated with different Policy objects that can be checked/enforced at varying enforcement levels. Through service APIs, a user can dynamically add Node to and remove node from a cluster, attach and detach policies, such as creation policy, deletion policy, load-balancing policy, scaling policy, health policy etc. Through integration with other OpenStack projects, users will be enabled to manage deployments and orchestrations large-scale resource pools much easier.

- Senlin is designed to be capable of managing different types of objects. An object’s lifecycle is managed using Profile Type implementations, which are plugins that can be dynamically loaded by the service engine.

### [1.2 Cài đặt và cấu hình](#install)
**Việc cài đặt sử dụng source code từ github**

- Clone source code:
    ```bash
    git clone https://github.com/openstack/senlin.git
    ```

- Cài đặt senlin:

    ```bash
    pip install -e .
    ```

- Cấu hình senlin:
    **Configure Reference: [Senlin Configuration](https://docs.openstack.org/senlin/latest/configuration/index.html)**
    *Thực hiện sửa file cấu hình /etc/senlin/senlin.conf*
    
    - Tạo service senlin:
        ```bash
        openstack service create senlin --type clustering
        ```

    - Tạo endpoint truy cập service senlin:
        ```bash
        openstack endpoint create senlin admin http://127.0.0.1:8777
        openstack endpoint create senlin internal http://127.0.0.1:8777
        openstack endpoint create senlin public http://127.0.0.1:8777
        ```
        thay thế: 127.0.0.1 bằng địa chị IP của host cài đặt senlin.

    - Tạo user truy cập service:
        ```bash
        openstack user create senlin --password Welcome123
        ```
    - Cấu hình database cho senlin service:
        ```bash
        [database]
        connection = mysql+pymysql://senlin:<MYSQL_SENLIN_PW>@127.0.0.1/senlin?charset=utf8
        ```

    - Cấu hình authentication;
        ```bash
        [keystone_authtoken]
        username = senlin
        password = Welcome123
        project_name = service
        project_domain_name = Default
        user_domain_name = Default
        auth_type = password
        service_token_roles_required = True
        auth_url = http://<HOST>5000/v3
        www_authenticate_uri = http://<HOST>5000/v3
        memcached_servers = <HOST>:11211

        [authentication]
        auth_url = http://<HOST>:5000/v3
        service_username = senlin
        service_password = Welcome123
        service_project_name = service
        ```

    - Cấu hình message dump:
        ```bash
        [oslo_messaging_notifications]
        driver = messaging
        ```

    - Cấu hình senlin API:
        ```bash
        [senlin_api]
        bind_port = 8777
        workers = 2
        ```
    
    - Cấu hình mặc định:
        ```bash
        [DEFAULT]
        log_dir = /var/log/senlin
        transport_url = rabbit://username:password@<HOST>
        health_check_interval_min = 30
        num_engine_workers = 2
        host = senlin
        default_action_timeout = 369
        event_dispatchers = database
        event_dispatchers = message
        ```

### [1.3 Kiểm tra](#result)
- Cài đặt CLI Client:
    - Tạo virtual environment:
        ```bash
        virtualenv env
        source env/bin/activate
        ```
    
    - Cài đặt openstack-client và python-senlinclient:
        ```bash
        # openstack client
        git clone https://github.com/openstack/python-openstackclient.git
        cd python-openstackclient
        pip install -e .

        # Install senlin client
        git clone https://github.com/openstack/python-senlinclient.git
        cd python-senlinclient
        pip install -e .
        ```

- Thực hiện kiểm tra bằng việc sử dụng câu lệnh:
    ```bash
    openstack cluster build info
    ```
    kết quả sẽ trả về tương tự như sau nếu thực hiện cài đặt thành công:
    ```bash
    +--------+---------------------+
    | Field  | Value               |
    +--------+---------------------+
    | api    | {                   |
    |        |   "revision": "1.0" |
    |        | }                   |
    | engine | {                   |
    |        |   "revision": "1.0" |
    |        | }                   |
    +--------+---------------------+
    ```