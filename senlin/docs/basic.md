### 2. Senlin Basic

### Nội dung
- [2.1 Tạo cluster](#cluster)
- [2.2 Thao tác với cluster](#cluster_op)

---
### [2.1 Tạo cluster](#cluster)
- Trước khi có thể tạo được một cluster, ta cần phải tạo ra một profile - có vai trò giống như một flavor trong Nova. Có vai trò là template để tạo ra các node trong senlin (tương ứng là members trong cluster khi tạo một cluster). Có nhiều loại profile khác nhau: 
    ```bash
    openstack cluster profile type list
    +----------------------------+---------+----------------------------+
    | name                       | version | support_status             |
    +----------------------------+---------+----------------------------+
    | container.dockerinc.docker | 1.0     | EXPERIMENTAL since 2017.02 |
    | os.heat.stack              | 1.0     | SUPPORTED since 2016.04    |
    | os.nova.server             | 1.0     | SUPPORTED since 2016.04    |
    +----------------------------+---------+----------------------------+
    ```
    mỗi một loại profile type là đại diện cho một loại node được tạo ra, một profile chỉ có thể khai báo một profile type. Ví dụ:
    ```bash
    type: os.nova.server
    version: 1.0
    properties:
      name: cirros_server
      flavor: 1
      image: "cirros-0.3.5-x86_64-disk"
      key_name: oskey
      networks:
       - network: private
      metadata:
        test_key: test_value
      user_data: |
        #!/bin/sh
        echo 'hello, world' > /tmp/test_file
    ```
    > *The above source file can be found in senlin source tree at /examples/profiles/nova_server/cirros_basic.yaml.*
    
    để có thể biết được chi tiết cách viết một profile, ta có thể sử dụng câu lệnh:
    ```bash
    openstack cluster profile type show name-version
    ```
    Ví dụ:
    ```bash
    openstack cluster profile type show os.nova.server-1.0
    ```
- Tạo một tệp tin có chưa nội dung của profile trên, sau đó tiến hành tạo profile với command:
    ```bash
    openstack cluster profile create --spec-file file_name cirros_server
    ```
- Tiến hành tạo một cluster với command:
    ```bash
    openstack cluster create --profile cirros_server mycluster
    ```
    một cluster sẽ được tạo ra nhưng không có bất kỳ members nào trong cluster. Nếu muốn thực hiện tạo members cùng với việc tạo cluster, ta sử dụng command:
    ```bash
    openstack cluster create --profile cirros_server --desired-capacity 2 mycluster
    ```
    cluster mới này sẽ được tạo ra và có 2 members (node) bên trong cluster.

---
### [2.2 Thao tác với cluster](#cluster_op)
##### 1. Scale cluster
Là action thực hiện scale out hoặc scale in cluster với subcommand là `shrink` và `expand`:
Để thực hiện scale out, ta sử dụng command:
```bash
openstack cluster expand mycluster
```
mặc định, mỗi lần thực hiện scale với câu lệnh trên, sẽ có một instance được tạo ra và thêm vào cluster. Để thay đổi, ta sử dụng option `--count` hoặc sử dụng `scaling policy` áp dụng cho cluster
Để thực hiện scale in, ta sử dụng command:
```bash
openstack cluster shrink --count 2 mycluster
```

##### 2. Resize cluster
Về bản chất thì là action thực hiện tương tự như scale out/in nhưng tiêu chí thực hiện thay đổi dựa trên việc tính toán:
Thay đổi số lượng members trong cluster tăng hoặc giảm theo tỉ lệ phần trăm, về một số lượng cụ thể hay một giá trị tương đối:
Để thực hiện, ta sử dụng subcommand `resize`. Ví dụ:
```bash
openstack cluster resize --capacity 2 mycluster
```
sẽ thực hiện tính toán để thay đổi kích thước của cluster sao cho chỉ có hai node là members của cluster tồn tại

##### 3. Create node
Node trong senlin tương ứng là một instance trong nova và có thể là một members của một cluster. Để thực hiện tạo ra một node, ta sử dụng subcommand:
```bash
openstack cluster node create --profile cirros_server mynode
```

##### 4. Adding a Node to a Cluster
senlin sẽ thực hiện tạo ra một node nhưng node nó lại không thuộc vào bất kỳ một cluster nào cả, chúng ta có thể thêm nó vào cluster với command:
```bash
openstack cluster members add --nodes mynode mycluster
```
hoặc có thể sử dụng command nhanh như sau:
```bash
openstack cluster node create --profile cirros_server --cluster mycluster mynode
```

##### 5. Removing a Node from a Cluster
Để thực hiện remove node ra khỏi cluster, ta sử dụng command: 
```bash
openstack cluster members del --nodes mynode mycluster
```
