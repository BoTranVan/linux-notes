### 3. Policy

### Nội dung
- [3.1 Giới thiệu về Policy](#about)
- [3.2 Thao tác với policy](#ops)
---
#### [3.1 Giới thiệu về Policy](#about)
A policy contains the set of rules that are checked/enforced before or after certain cluster operations are performed. The detailed specification of a specific policy type is provided as the spec of a policy object when it is created. The following is a sample spec for a deletion policy:
```bash
# Sample deletion policy that can be attached to a cluster.
type: senlin.policy.deletion
version: 1.0
description: A policy for choosing victim node(s) from a cluster for deletion.
properties:
  # The valid values include:
  # OLDEST_FIRST, OLDEST_PROFILE_FIRST, YOUNGEST_FIRST, RANDOM
  criteria: OLDEST_FIRST

  # Whether deleted node should be destroyed 
  destroy_after_deletion: True

  # Length in number of seconds before the actual deletion happens
  # This param buys an instance some time before deletion
  grace_period: 60

  # Whether the deletion will reduce the desired capacity of
  # the cluster as well
  reduce_desired_capacity: False
```
> *The above source file can be found in senlin source tree at /examples/policies/deletion_policy.yaml.*
tạo một tệp tin có chưa nội dung policy như ở trên. Sau đó thực hiện tạo policy với command:
```bash
openstack cluster policy create --spec-file file_name policy_name
```

Để biết được các loại policy được hỗ trợ, ta sử dụng command:
```bash
openstack cluster policy type list
+--------------------------------+---------+----------------------------+
| name                           | version | support_status             |
+--------------------------------+---------+----------------------------+
| senlin.policy.affinity         | 1.0     | SUPPORTED since 2016.10    |
| senlin.policy.batch            | 1.0     | EXPERIMENTAL since 2017.02 |
| senlin.policy.deletion         | 1.0     | SUPPORTED since 2016.04    |
| senlin.policy.deletion         | 1.1     | SUPPORTED since 2018.01    |
| senlin.policy.health           | 1.0     | EXPERIMENTAL since 2017.02 |
|                                |         | SUPPORTED since 2018.06    |
| senlin.policy.health           | 1.1     | SUPPORTED since 2018.09    |
| senlin.policy.loadbalance      | 1.0     | SUPPORTED since 2016.04    |
| senlin.policy.loadbalance      | 1.1     | SUPPORTED since 2018.01    |
| senlin.policy.region_placement | 1.0     | EXPERIMENTAL since 2016.04 |
|                                |         | SUPPORTED since 2016.10    |
| senlin.policy.scaling          | 1.0     | SUPPORTED since 2016.04    |
| senlin.policy.zone_placement   | 1.0     | EXPERIMENTAL since 2016.04 |
|                                |         | SUPPORTED since 2016.10    |
+--------------------------------+---------+----------------------------+
```
để có thể biết được chi tiết cách viết một policy, ta có thể sử dụng câu lệnh:
```bash
openstack cluster policy type show name-version
```
---
#### [3.2 Thao tác với policy](#ops)

##### Attaching a Policy
The enforce a policy on a cluster, attach a policy to it:
```bash
openstack cluster policy attach --policy dp01 mycluster
```
To verify the policy attach operation, do the following:
```bash
openstack cluster policy binding list mycluster
openstack cluster policy binding show --policy dp01 mycluster
```

##### Verifying a Policy
To verify the deletion policy attached to the cluster mycluster, you can try expanding the cluster, followed by shrinking it:
```bash
openstack cluster members list mycluster
openstack cluster expand mycluster
openstack cluster members list mycluster
openstack cluster shrink mycluster
openstack cluster members list mycluster
```

##### Detaching a Policy
The enforce a policy from a cluster, detach a policy to it:
```bash
openstack cluster policy detach --policy dp01 mycluster
```
To verify the policy detach operation, do the following:
```bash
openstack cluster policy binding list mycluster
```