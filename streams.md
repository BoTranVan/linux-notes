- Have 3 standard file streams:

    + Standard input (stdin) have descriptor is 0
    + Standard output (stdout) have descriptor is 1
    + Standard error (stderr) have descriptor is 2


- stdin: your keyboard
- stdout, stderr: printed in your terminal


- stdin directed input to a file or from ouput of a command through pipe.
- stdout often redirected to a file
- often, stderr redirected to error logging file because stderr write error messages.


- Using `>` redirecting stdout
- Using `<` redirecting stdin


- Example:

    - Redirecting by: `>` and `<`

        `>` override data in file and `>>` insert data to end file.

                echo "Hihi" > file.txt

                cat < file.txt


    - Write data to filename:

            cat > filename << EOF

        to stop write, input EOF, with EOF is text using stop write data

            cat > filename << STOP

        like:

            cat << EOF > filename

                content

            EOF

    - `<<`: redirecting input 'a here documents' - read multiple lines
    - `<<<`: redirecting input 'a here string' - read single line



    - Redirect stdout and stderr to a file like:

        + Run commnad:

                cat << EOF > filename.sh
                #!/bin/bash
                # filename.sh

                echo 'something'
                EOF

        + redirecting stdout to a file:

                cat filename.sh 1> out

            `1>` is default so can using `>`. Like `cat filename.sh > out`

        + redirecting stderr to a file:

                cat filename.sh 2> err

        + redirecting stdout and stderr to a file:

                cat filename.sh &> out.err




    - Redirecting by pipe: |

        + redirecting stdout:

                cat filename.sh | sh

                cat filename.sh s | tee -a xxx

        + redirecting stderr and stdout

                cat filename.sh s |& tee -a xxx