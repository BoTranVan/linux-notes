# Chapter 1


Keystone: Indentity service

Swift: Object storage service

Cider: Block storage service

Manila: File-based access

Glance: Image registry

Nova COmpute: compute service in OpenStack and manages virtual machines in response to service requests )_

- Nova-api: accept and respond request via api
- Nova-Compute: creates and terminates VM instances via the hypervisor's APIs
- Nova-network: accepts networking tasks from the queue and then performs these tasks to manipulate the network (Neutron)
- Nova-scheduler: takes a VM instance's request from the queue and determines where it should run

- Neutron: Network service
    - Ports
    - Networks
    - Subnets
    - Routers
    - Private IP:
        - tenant's networks
        - external networks
        - floating ip
        - LBaaS
        - FWaaS
        - VPNaaS

- Architecture of Neutron:

    - Server: accept api and routes to appropriate pluggin.
    - Plugin: e plugging in or unplugging ports, creating networks and subnets, or IP addressing.
    - agents: receive commands from the plugins on the Neutron server and bring the changes into effect on the individual compute or network nodes.

=> manages network connectivity between the OpenStack instances
provide a way to integrate vendor networking solutions and a flexible way to extend network services.
allows users to manage and create
networks or connect servers and nodes to various networks.








------------------------------------------------------
------------------------------------------------------

Ceilometer, Aodh, and Gnocchi -
Telemetry

------------------------------------------------------
------------------------------------------------------

- Ceilometer: metering resource utilization such as VMs, disks, networks, routers, and so on
    allows data collection from various sources, such as the message bus,
    polling resources, centralized agents, and so on.

- Aodh: managing alarms and triggering
them based on collected metering and scheduled events

- Heat - Orchestration


workflow based on API calls in OpenStack

- Calling the identity service for authentication
- Generating a token to be used for subsequent calls
- Contacting the image service to list and retrieve a base image
- Processing the request to the compute service API
- Processing compute service calls to determine security groups and keys
- Calling the network service API to determine available networks
- Choosing the hypervisor node by the compute scheduler service
- Calling the block storage service API to allocate volume to the instance
- Spinning up the instance in the hypervisor via the compute service API call
- Calling the network service API to allocate network resources to the
  instance