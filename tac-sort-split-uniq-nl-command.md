# Use ‘tac’, ‘sort’, ‘split’, ‘uniq’, ‘nl’

1. `tac command`:

- Same as `cat command` but it print to standard with order reverse. Example:

        cat filename.sh
        1
        2
        3
        4
        5

    and

        tac filename.sh

    will printed:

        5
        4
        3
        2
        1

2. `sort command`:

- Using to sort line in a text file base on digits in line. Example:

        cat filename.sh
        a
        b
        c
        d
        e
        f
        g
        w
        t

    and

        sort filename.sh

    will printed:

        a
        b
        c
        d
        e
        f
        g
        t
        w

- Default, sort will print with alpha-beta. To print with reverse order, we using `-r` option. Example:

        sort - r filename.sh

    will printed:

        w
        t
        g
        f
        e
        d
        c
        b
        a


- In stdout of `sort command`, each position seperate by a `space`. Example:

        ls -al /etc/ssh | sort

    will be printed like:

        -rw-------  1 root root    227 Dec 14 01:20 ssh_host_ecdsa_key
        -rw-------  1 root root    399 Dec 14 01:20 ssh_host_ed25519_key
        -rw-------  1 root root    668 Dec 14 01:20 ssh_host_dsa_key
        -rw-------  1 root root   1679 Dec 14 01:20 ssh_host_rsa_key
        -rw-r--r--  1 root root     93 Dec 14 01:20 ssh_host_ed25519_key.pub
        -rw-r--r--  1 root root    173 Dec 14 01:20 ssh_host_ecdsa_key.pub
        -rw-r--r--  1 root root    338 Dec 14 01:21 ssh_import_id
        -rw-r--r--  1 root root    393 Dec 14 01:20 ssh_host_rsa_key.pub
        -rw-r--r--  1 root root    601 Dec 14 01:20 ssh_host_dsa_key.pub
        -rw-r--r--  1 root root   1756 Aug 11  2016 ssh_config
        -rw-r--r--  1 root root   2528 Dec 14 01:21 sshd_config
        -rw-r--r--  1 root root 300261 Aug 11  2016 moduli
        drwxr-xr-x  2 root root   4096 Dec 14 01:21 .
        drwxr-xr-x 84 root root   4096 Apr 24 01:03 ..
        total 348

    In line 1, we see 9 columns. Using `-n option` and `-k option` to sort with number value in column. Example:

        ls -al /etc/ssh/ | sort -nk 5

    will be printed:

        -rw-r--r--  1 root root     93 Dec 14 01:20 ssh_host_ed25519_key.pub
        -rw-r--r--  1 root root    173 Dec 14 01:20 ssh_host_ecdsa_key.pub
        -rw-------  1 root root    227 Dec 14 01:20 ssh_host_ecdsa_key
        -rw-r--r--  1 root root    338 Dec 14 01:21 ssh_import_id
        -rw-r--r--  1 root root    393 Dec 14 01:20 ssh_host_rsa_key.pub
        -rw-------  1 root root    399 Dec 14 01:20 ssh_host_ed25519_key
        -rw-r--r--  1 root root    601 Dec 14 01:20 ssh_host_dsa_key.pub
        -rw-------  1 root root    668 Dec 14 01:20 ssh_host_dsa_key
        -rw-------  1 root root   1679 Dec 14 01:20 ssh_host_rsa_key
        -rw-r--r--  1 root root   1756 Aug 11  2016 ssh_config
        -rw-r--r--  1 root root   2528 Dec 14 01:21 sshd_config
        drwxr-xr-x  2 root root   4096 Dec 14 01:21 .
        drwxr-xr-x 84 root root   4096 Apr 24 01:03 ..
        -rw-r--r--  1 root root 300261 Aug 11  2016 moduli

- Shoud try run command: `ls -al /etc/ssh/ | sort -k 9`

- Using `-u option` to remove duplicates. Exapmle:

        ls -al /etc/ssh/ | sort -nk 5 -u

3. `split command`:

- Using to split a file into pieces. Example:

        cat << EOF > filename.sh
        1
        2
        3
        4
        5
        6
        7
        8
        9
        10

    After, run command:

        split filname -l 5

    we will see in this folder have 5 new files with format: xa*.

- Options:

    - `-l: split by lines number to file`
    - `-b: put bytes to each output file`
    - `-n: split to n files`

3. `uniq command`: Report or omit duplicate files.

    ```
    cat filename.sh
    1
    2
    1
    1
    3
    4
    5
    1

    1
    1


    1
    ```

    After run `uniq filename.sh`. We will see:

    ```
    1
    2
    1
    3
    4
    5
    1

    1

    1
    ```

- Using:

    - `-c option`: to show prefix lines by the number of occurrences
    - `-d option`: to only print duplicate lines
    - `-f N option`: to avoid comparing the first N fields. (`cat -n filename.sh > xxx && uniq -f 1 xxx`)
    - `-u option`: to only print unique lines


4. `nl command`: show number lines of files igone empty lines (same as `cat command with -n option`). Example:

        `nl filename.sh`

    will be printed:

    ```
     1  1
     2  2
     3  1
     4  1
     5  3
     6  4
     7  5
     8  1

     9  1
    10  1


    11  1
    ```