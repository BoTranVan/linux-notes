# About permission


- Permission in Linux have types: `read`, `write`, `execute` and `don't permission`know as value `4`, `2`, `1`, `0` or `w`, `r` and `x`.
- Have 4 permission groups: `owner`, `group`, `all user` used are:

    |Permission Groups|Present as|
    |----------------|-----------|
    |owner|u|
    |group|g|
    |all user|a|
    |other|o|

- Example:

        chmod a+x filename.sh

    with:

        -rw-r--r--. 1 root root 0 May  7 05:29 filename.sh

    we change permission of filename.sh being:

        -rwxr-xr-x. 1 root root 0 May  7 05:29 filename.sh

    and:

        chmod a-x filename.sh

    we change permission of filename.sh being:

        -rw-r--r--. 1 root root 0 May  7 05:29 filename.sh

- Used `a+x` equal with `111`