# About RAID Configurations

1. Level of RAID commonly and Characteristics write data

- `RAID 0`: `Striping`: splits data across more than one drive. If any drive fails, all drives in the stripe also fail because there is no redundancy
- `RAID 1`: `Mirroring`: `replicates all data on one drive to a second peer-level drive. If one drive fails, the other drive is not affected.`
- `RAID 5`: `Striping with Distributed Parity`: writes data and parity information in stripes along three or more drives. the data on that drive can be restored using all the other drives in the array along with the parity bits that were created when the data was stored
- `RAID 6`: `Striped set with dual distributed parity`: adds an extra layer of parity over RAID 5
- `RAID 10`: `Striping across mirrors`: combines the striping of RAID 0 and with the mirroring of RAID 1


2. Drive configurations and RAID levels

    |RAID Levels|Number of drives|
    |-----------|----------------|
    | 0| Any |
    | 1| Only 2 drives |
    | 5| Minimum 3 drives |
    | 6| Minimum 4 drives |
    | 10| Minimum of 4 drives, and number addition drivers must is multiple of 2 (4, 6, 8, ... n*2) |



3. Create a RAID

- Set RAID flag:

        parted --script /dev/sdb "mklabel gpt"

        parted --script /dev/sdb "mkpart primary 0% 100%"

        parted --script /dev/sdb "set 1 raid on"

-   Configure RAID 1:

        mdadm --create /dev/md0 --level=raid1 --raid-devices=2 /dev/sdb1 /dev/sdc1

    when run command: `cat /proc/mdstat ` will be printed:

        Personalities : [raid1]
        md0 : active raid1 sdc1[1] sdb1[0]
              20953088 blocks super 1.2 [2/2] [UU]
              [================>....]  resync = 83.0% (17408640/20953088) finish=0.9min speed=63130K/sec

        unused devices: <none>

    and after:

        Personalities : [raid1]
        md0 : active raid1 sdc1[1] sdb1[0]
              20953088 blocks super 1.2 [2/2] [UU]

        unused devices: <none>


- When /dev/sdc1 being error. Run command to resync:

        mdadm --manage /dev/md0 --add /dev/sdc1
        
- Save configure:
        
        sudo mdadm --detail --scan | sudo tee -a /etc/mdadm/mdadm.conf

