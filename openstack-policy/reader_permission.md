### Nova
- List port interfaces or show details of a port interface attached to a server
- Show console output for a server
- Show console details for a server instance
- List flavor access information
- Show an extra spec for a flavor
- List floating IP pools. This API is deprecated.
- List actions and show action details for a server.
- Show IP addresses details for a network label of a server
- List IP addresses that are assigned to a server
- List networks for the project and show details for a network.
- List all metadata of a server
- Show metadata for a server
- List all tags for given server
- List all servers with detailed information
- List all servers
- Show a server
- Show usage statistics for a specific tenant
- List volume attachments for an instance
- Show details of a volume attachment



### Cinder
- List messages.
- Show message.
- Show snapshot's metadata or one specified metadata with a given key.
- List snapshots.
- List snapshots with extended attributes.
- Show snapshot.
- List backups.
- Show backup.
- List group snapshots.
- Show group snapshot.
- List volume transfer.
- Show one specified volume transfer.
- Show volume.
- List volumes.
- List or show volume with tenant attribute.
- List groups.
- Show group.
