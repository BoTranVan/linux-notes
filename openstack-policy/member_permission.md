### Nova
- List port interfaces or show details of a port interface attached to
- Attach an interface to a server
- Detach an interface from a server
- Show console output for a server
- Create a console for a server instance
- Show console details for a server instance
- Delete a console for a server instance
- List all consoles for a server instance
- Create a back up of a server
- List flavor access information
- Show an extra spec for a flavor
- List floating IP pools. This API is deprecated.
- List actions and show action details for a server.
- Show IP addresses details for a network label of a server
- List IP addresses that are assigned to a server
- List all keypairs
- Create a keypair
- Delete a keypair
- Show details of a keypair
- Lock a server
- Unlock a server
- Lock a server
- Unlock a server
- List networks for the project and show details for a network.
- Pause a server
- Unpause a paused server
- Generate a URL to access remote server console
- Rescue/unrescue a server
- List, show, add, or remove security groups.
- List all metadata of a server
- Show metadata for a server
- Create metadata for a server
- Replace metadata for a server
- Update metadata from a server
- Delete metadata from a server
- Delete all the server tags
- List all tags for given server
- Replace all tags on specified server with the new set of tags.
- Delete a single tag from the specified server
- Add a single tag to the server if server has no specified tag
- List all servers
- List all servers with detailed information
- Show a server
- Create a server
- Create a server with the requested volume attached to it
- Create a server with the requested network attached to it
- Create a server with trusted image certificate IDs
- Creating a server with a flavor that has 0 disk
- edited
- Delete a server
- Update a server
- Confirm a server resize
- Revert a server resize
- Reboot a server
- Resize a server
- Rebuild a server
- Rebuild a server with trusted image certificate IDs
- Create an image from a server
- Create an image from a volume backed server
- Start a server
- Stop a server
- Trigger crash dump in a server
- Shelve server
- Unshelve (restore) shelved server
- Shelf-offload (remove) server
- Show usage statistics for a specific tenant
- Resume suspended server
- Suspend server
- Manage volumes for use with the Compute API.
- List volume attachments for an instance
- Attach a volume to an instance
- Show details of a volume attachment

### Cinder
- Create attachment.
- Update attachment.
- Delete attachment.
- Mark a volume attachment process as completed (in-use)
- Allow multiattach of bootable volumes.
- List messages.
- Show message.
- Delete message.
- Show snapshot's metadata or one specified metadata with a given key.
- Update snapshot's metadata or one specified metadata with a given key
- Delete snapshot's specified metadata with a given key.
- List snapshots.
- List snapshots with extended attributes.
- Create snapshot.
- Show snapshot.
- Update snapshot.
- Delete snapshot.
- Update database fields of snapshot.
- List backups.
- Create backup.
- Show backup.
- Update backup.
- Delete backup.
- Restore backup.
- List groups.
- Show group.
- List group snapshots.
- Create group snapshot.
- Show group snapshot.
- Delete group snapshot.
- Update group snapshot.
- Extend a volume.
- Extend a attached volume.
- Revert a volume to a snapshot.
- Upload a volume to image.
- Initialize volume attachment.
- Terminate volume attachment.
- Roll back volume status to 'in-use'.
- Mark volume as reserved.
- Unmark volume as reserved.
- Begin detach volumes.
- Add attachment metadata.
- Clear attachment metadata.
- List volume transfer.
- Create a volume transfer.
- Show one specified volume transfer.
- Accept a volume transfer.
- Delete volume transfer.
- Create volume metadata.
- Update volume's metadata or one specified metadata with a given key.
- Delete volume's specified metadata with a given key.
- Volume's image metadata related operation, create, delete, show and list
- Create volume.
- Create volume from image.
- Show volume.
- List volumes.
- Update volume.
- Delete volume.
- List or show volume with tenant attribute.
- Show volume's encryption metadata.
- Create multiattach capable volume.
