# Cấu hình Elasticsearch sử dụng Redis broker làm cache trong môi trường Openstack

____

# Mục lục


- [1. Giới thiệu về các thành phần](#about)
- [2. Mô hình thực hiện cài đặt](#model-config)
- [3. Thực hiện cấu hình](#config)
	- [3.1 Cấu hình elasticsearch](#config-elasticsearch)
	- [3.2 Cấu hình cho logstash](#config-logstash)
	- [3.3 Cấu hình cho redis](#config-redis)
	- [3.4 Cấu hình cho beats (filebeat)](#config-filebeat)
	- [3.5 Cấu hình để sử dụng virtual-server](#config-virtual-server)
- [4. Kiểm tra](#check)
- [Các nội dung khác](#content-others)

____

# <a name="content">Nội dung</a>

- ### <a name="about">1. Giới thiệu về các thành phần</a>
	- Elasticsearch

		Elasticsearch là một RESTful distributed search engine. Có thể hiểu là nó cung cấp khả năng tìm kiếm phân tán qua API. Lưu trữ dữ liệu theo dạng NoSQL database (cơ sở dữ liệu không có cấu trúc). Mà điểm mạnh của nó là việc tìm kiếm các dữ liệu rất nhanh.
	- Logstash

		Logstash có chức năng phân tích cú pháp của các dòng dữ liệu. Việc phân tích làm cho dữ liệu đầu vào ở một dạng khó đọc, chưa có nhãn thành một dạng dữ liệu có cấu trúc, được gán nhãn.

	- Kibana

		Kibana được phát triển riêng cho ứng dụng ELK, cung cấp giao diện web và có thể thực hiển chuyển đổi các truy vấn của người dùng thành câu truy vấn mà Elasticsearch có thể thực hiện được. Kết quả hiển thị bằng nhiều cách: theo các dạng biểu đồ.

	- Beats(Filebeat)

		Thực hiện thu thập log và gửi lên cho Logstash. Có thể coi đây như là agents.

- ### <a name="model-config">2. Mô hình thực hiện cài đặt</a>

	![model-settings](images/felk.png)

	Trong mô hình trên, bao gồm các thành phần sau:
		1. Beats(Filebeat) có tác dụng thu thập log từ các thiết bị cần giám sát.
		2. Redis-server có tác dụng làm message broker cho các log mà beats gửi lên
		3. Logstash thực hiện lấy log từ redis-server và gửi lên elasticsearch.
		4. Elasticsearh & kibana có tác dụng xử lý log từ logstash và hiển thị lên giao diện web.

	Các máy sử dụng hệ điều hành Ubuntu 16.04.05 (Xenial).

- ### <a name="config">3. Thực hiện cấu hình</a>
	Đầu tiên, trước khi tiến hành cài đặt, ta cần phải thực hiện cài đặt Java Enviroment trên tất cả các máy với câu lệnh:

		apt install -y openjdk-8-jre-headless

	thêm repository của elastic.co để có thể cài đặt các package từ apt manages với câu lệnh:

		wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
		sudo apt-get install apt-transport-https
		echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list
		sudo apt-update

	- ### <a name="config-elasticsearch">3.1 Cấu hình elasticsearch</a>
		- Để cài đặt elasticsearch, ta sử dụng câu lệnh:

				sudo apt-get install -y elasticsearch
				service elasticsearch start
				systemctl enable elasticsearch

			sau khi thực hiện cài đặt thành công elasticsearch trên các node cần thiết, ta tiến hành thực hiện cấu hình cluster cho elasticsearch bằng việc sửa nội dung của file `/etc/elasticsearch/elasticsearch.yml` với nội dung như sau:

				cluster.name: my-elasticsearch-cluster
				node.name: ${HOSTNAME}
				network.host: 0.0.0.0
				http.port: 9200
				discovery.zen.ping.unicast.hosts: ["10.5.9.179", "10.5.8.181", "10.5.10.20"]
				discovery.zen.minimum_master_nodes: 2

			trong đó:
			"10.5.9.179", "10.5.8.181", "10.5.10.20" lần lượt là địa chỉ IP của các node cài đặt Elasticsearch.

			và thêm nội dung sau vào cuối file:

				# Enable auto index
				action.auto_create_index: true

				# Set to false to disable X-Pack graph features.
				xpack.graph.enabled : true

				# Set to false to disable X-Pack machine learning features.
				xpack.ml.enabled : true

				# Set to false to disable X-Pack monitoring features.
				xpack.monitoring.enabled : true

				# Set to false to disable X-Pack security features.
				#xpack.security.enabled : false

				# Set to false to disable Watcher.
				xpack.watcher.enabled : true

			lưu lại cấu hình vừa xong và restart lại service Elasticsearch với câu lệnh:

				service elasticsearch restart

	- ### <a name="config-logstash">3.2 Cấu hình cho logstash</a>
		- Để cài đặt logstash, ta thực hiện sử dụng câu lệnh sau:

				apt-get install logstash
				service logstash start
				systemctl enable logstash

		- Sửa nội dung của file `/etc/logstash/logstash.yml` với nội dung sau:

				node.name: logstash_01
				path.data: /var/lib/logstash
				pipeline.id: main
				pipeline.workers: 2

				http.host: "0.0.0.0"
				http.port: 9600-9700
				path.logs: /var/log/logstash
				xpack.monitoring.enabled: true
				xpack.monitoring.elasticsearch.url: ["http://10.5.9.179:9200", "http://10.5.8.181:9200", "http://10.5.10.20:9200"]

			trong đó: "http://10.5.9.179:9200", "http://10.5.8.181:9200", "http://10.5.10.20:9200" là địa chỉ truy cập elasticsearch tới các node đã cài elasticsearch.

		- Sửa hoặc thêm nội dung sau vào file `/etc/logstash/conf.d/input-redis-filebeat.conf`:

				input {
					redis {
						host => '10.5.10.96'
						key => 'filebeat'
						data_type => 'list'
						#password => 'your_password'
						codec => json
					}
				}
				output {
					elasticsearch {
						hosts => ["http://10.5.9.179:9200", "http://10.5.8.181:9200", "http://10.5.10.20:9200"]
						index => "%{[@metadata][beat]}-%{[@metadata][version]}-%{+YYYY.MM.dd}"
						#user => "elastic"
						#password => "changeme"
					}
				}

			nội dung cấu hình trên đơn giản là sẽ lấy dữ liệu log từ redis-cluster với địa chỉ truy cập là `10.5.10.96` có key là `filebeat` sau đó sẽ gửi lên các server cài đặt elasticsearch.

	- ### <a name="config-redis">3.3 Cấu hình cho redis</a>
		- Thực hiện sử dụng câu lệnh sau trên tất cả các node sẽ cài đặt redis-server:

				apt install -y redis-server pacemaker corosync fence-agents haproxy

		- Sau khi thực hiện cài đặt xong, ta tiến hành cấu hình redis-server sử dụng mode `sentinel` tời file `/etc/redis/sentinel.conf` như sau:

				bind node_addr 127.0.0.1
				port 26379
				dir "/var/lib/redis"
				logfile "/var/log/redis/redis-sentinel.log"
				sentinel monitor mymaster node_master_addr 6379 2
				sentinel down-after-milliseconds mymaster 5000
				sentinel failover-timeout mymaster 10000
				sentinel config-epoch mymaster 12
		
			trong đó: `node_addr` là địa chỉ IP của node đang cấu hình(nếu có bind 127.0.0.1 nữa thì ta cần phải cho địa chỉ này về phía sau.), `node_master_addr` là địa chỉ IP của node redis-server master.

		- Tiếp theo, cần phải tiến hành cấu hình cho redis-server bằng việc thêm hoặc sửa nội dung sau tới file `/etc/redis/redis.conf` như sau:
			- Trên Master node:

					bind node_addr 127.0.0.1
					port 6379
					timeout 60
					tcp-keepalive 60
					appendonly no

			- Trên Slave node:

					bind node_addr 127.0.0.1
					port 6379
					timeout 60
					tcp-keepalive 60
					appendonly no
					slaveof node_master_addr 6379

		
			trong đó: `node_addr` là địa chỉ IP của node đang cấu hình(nếu có bind 127.0.0.1 nữa thì ta cần phải cho địa chỉ này về phía sau.), `node_master_addr` là địa chỉ IP của node redis-server master.

		- Tiếp theo, trên tất cả các node sau khi đã cài đặt và cấu hình cho redis-server, ta cần tạo ra một file `init service` `/lib/systemd/system/redis-sentinel.service` với nội dung như sau:

				[Unit]
				Description=Advanced key-value store
				After=redis-server.service
				Documentation=http://redis.io/documentation, man:redis-server(1)

				[Service]
				Type=forking
				ExecStart=/usr/bin/redis-server /etc/redis/sentinel.conf --sentinel
				PIDFile=/var/run/redis/redis-sentinel.pid
				TimeoutStopSec=0
				Restart=always
				User=root
				Group=root

				ExecStop=/bin/kill -s TERM 

				[Install]
				WantedBy=multi-user.target

			sửa nội dung file `/lib/systemd/system/redis-server.service`
			
				[Unit]
				...
				Requires=redis-sentinel.service

		- Tiếp theo, ta cần cấu hình cluster cho các redis-server bằng việc thêm hoặc sửa nội dung file `/etc/corosync/corosync.conf` như sau:

				totem {
					version: 2
					secauth: off
					cluster_name: redis_cluster
					transport: udpu
					rrp_mode: active
					interface {
					ringnumber: 0
					bindnetaddr: 10.5.8.0
					broadcast: yes
					mcastport: 5405
					}
				}

				nodelist {
					node {
						ring0_addr: 10.5.10.64
						nodeid: 101
					}
					node {
						ring0_addr: 10.5.8.114
						nodeid: 102
					}
					node {
						ring0_addr: 10.5.10.82
						nodeid: 103
					}
				}

				quorum {
				provider: corosync_votequorum
				two_node: 1
				wait_for_all: 1
				last_man_standing: 1
				auto_tie_breaker: 0
				}

			trong đó: "10.5.10.64", "10.5.8.114" và "10.5.10.82" là các IP của node cài đặt redis-server.

		- Khởi động lại corosync service để update cấu hình mới với câu lệnh:

				service corosync restart
				service pacemaker restart

			kiểm tra với câu lệnh `crm` để kiểm tra kết quả cho việc cấu hình cluster. Kết quả sẽ hiển thị tượng tự như sau:

				Last updated: Tue Oct 16 17:18:31 2018          Last change: Fri Oct 12 17:39:27 2018 by root via cibadmin on botv-redis-01
				Stack: corosync
				Current DC: botv-redis-02 (version 1.1.14-70404b0) - partition with quorum
				3 nodes and 5 resources configured

				Online: [ botv-redis-01 botv-redis-02 botv-redis-03 ]

				Full list of resources:

			tiếp theo, ta tạo ra một virtual ip để cung cấp cho việc truy cập redis-server với câu lệnh sau:

				crm config primitive virtual_ip IPaddr2 \
				params ip=10.5.10.96 cidr_netmask=24 \
				meta failure-timeout=60s resource-stickiness=100

			trong đó "10.5.10.96" là địa chỉ virtual ip ta cần tạo ra.

		- Tiếp theo, ta cần cấu hình HAProxy bằng việc thêm hoặc sửa nội dung sau tới file `/etc/haproxy/haproxy.cfg` trên các node cài redis-server:

				listen redis_sentinel
				bind 10.5.10.96:6379
				balance  source
				mode tcp
				option  tcpka
				option  tcp-check
				option  tcplog
				tcp-check connect
				tcp-check send PING\r\n
				tcp-check expect string +PONG
				tcp-check send info\ replication\r\n
				tcp-check expect string role:master
				tcp-check send QUIT\r\n
				tcp-check expect string +OK
				server redis01 10.5.10.64:6379 check inter 2000 rise 2 fall 5
				server redis02 10.5.8.114:6379 check inter 2000 rise 2 fall 5
				server redis03 10.5.10.82:6379 check inter 2000 rise 2 fall 5
			
			trong đó "10.5.10.96" là địa chỉ virtual_ip vừa tạo ra ở trên.

		-Sau khi cấu hình HAProxy xong, ta cần tạo resource cho HAProxy với câu lệnh sau:

				crm config primitive haproxy lsb:haproxy \
				op monitor interval=3s

			tiếp theo, ta sẽ tạo resource cho redis-server với câu lệnh sau:

				crm config primitive redis systemd:redis-server \
				op monitor interval=3s
				crm config clone redis-server-clone redis-server

		- Cuối cùng, ta cần tạo các constraint cho các resource đã tạo với câu lệnh sau:

				crm config colocation vip-with-haproxy inf: virtual_ip haproxy
				crm config order haproxy-after-vip Mandatory: virtual_ip haproxy

			cấu hình trên có tác dụng đảm bảo virtual_ip và haproxy luôn chạy cùng nhau trên một node.

	- ### <a name="config-filebeat">3.4 Cấu hình cho beats (filebeat)</a>
		- Để cài đặt filebeat, ta sử dụng câu lệnh sau:

				apt install -y filebeat

			tiếp tục, ta cần cấu hình cho filebeat bằng việc thực hiện các bước sau:
			- Enable module system:

					filebeat modules enable system

			- Cấu hình filebeat get log của Keystone bằng việc sửa nội dung file `/etc/filebeat/modules.d/system.yml`:

					- module: system
					# Syslog
					syslog:
						enabled: true

						# Set custom paths for the log files. If left empty,
						# Filebeat will choose the paths depending on your OS.
						var.paths:
							- /var/log/keystone/keystone.log

						# Convert the timestamp to UTC. Requires Elasticsearch >= 6.1.
						#var.convert_timezone: false

					# Authorization logs
					auth:
						enabled: true

						# Set custom paths for the log files. If left empty,
						# Filebeat will choose the paths depending on your OS.
						var.paths:
							- /var/log/*.log
						# Convert the timestamp to UTC. Requires Elasticsearch >= 6.1.
						#var.convert_timezone: false

			- Cấu hình ouput filebeat tới redis-server bằng việc thêm nội dung sau vào file `/etc/filebeat/filebeat.yml`:

					output.redis:
					hosts: ["10.5.10.96"]
					#password: "my_password"
					key: "filebeat"
					db: 0
					timeout: 60
			
			- Khởi động lại service filebeat với câu lệnh:

					service filebeat restart

	- ### <a name="config-virtual-server">3.5 Cấu hình để sử dụng virtual-server</a>
		- Tương tự như cấu hình cluster cho redis-server, ta cần phải thực hiện cài đặt với câu lệnh sau trên các node cài đặt elasticsearch:

				apt install pacemaker corosync fence-agents nginx

		- Sau khi thực hiện cấu hình pacemaker và corosync tương tự như đã cấu hình cho redis, ta thực hiện cấu hình virtual-server như sau trên các node cài đặt elasticsearch với câu lệnh:

				cut << EOF > /etc/nginx/sites-enabled/kibana.conf
				upstream kibana {
					server 10.5.9.179:5601;
					server 10.5.8.181:5601;
					server 10.5.10.20:5601;
				}
				server {
					listen 5601 default_server;
					listen [::]:5601 default_server;
					server_name _;
					location / {
						proxy_pass http://kibana;
					}
				}

		- Tiếp theo, ta cần cấu hình cho kibana như sau trên tất cả các node đã cài đặt kibana:
			trong file `/etc/kibana/kibana.yml`:

				server.port: 5601
				server.host: "0.0.0.0"
				server.name: "kibana_01"
				elasticsearch.url: "http://localhost:9200"
				kibana.index: ".kibana"
				kibana.defaultAppId: "home"
			
		Cuối cùng, ta tạo ra resource cho cluster này:
		- virtual_ip:

				crm config primitive virtual_ip IPaddr2 \
				params ip=10.5.10.96 cidr_netmask=24 \
				meta failure-timeout=60s resource-stickiness=100
			
		- nginx:

				crm configure primitive nginx ocf:heartbeat:nginx \
				params httpd="/usr/sbin/nginx" \
				op start timeout="40s" interval="0" \
				op monitor timeout="30s" interval="10s" on-fail="restart" \
				op stop timeout="60s" interval="0"

		- constraint:

				crm config colocation vip-with-nginx inf: virtual_ip nginx
				crm config order nginx-after-vip Mandatory: virtual_ip nginx 

- ### <a name="check">4. Kiểm tra</a>
	- Trong mỗi trường Openstack, để tạo ra một địa chỉ IP sử dụng cho các cluster, ta cần phải thực hiện sử dụng câu lệnh sau:
	
			openstack port create --fixed-ip subnet=22,ip-address=10.5.10.96 \
			--network network_name \
			--allowed-address ip-address=10.5.10.64 \
			--allowed-address ip-address=10.5.8.114 \
			--allowed-address ip-address=10.5.10.82 \
			virtual_ip_redis

		trong đó: `network_name` là tên network sẽ tạo ra địa chỉ IP cần tạo là "10.5.10.96" và địa chỉ này có thể sử dụng cho một trong các host có địa chỉ IP là "10.5.10.64", "10.5.8.114" và "10.5.10.82".
	Như vậy, với 2 cluster đã tạo ở trên, ta cần phải tạo ra 2 địa chỉ IP với câu lệnh tương tự như ở trên.




	


____

# <a name="content-others">Các nội dung khác</a>
