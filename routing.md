# Set IP static in LINUX

- Use `nmcli`, `nmtui`, `ip` and `ifconfig`


- With nmcli

        nmcli deivce modify ens36 ipv4.adress 10.10.10.72/24 ... [ip4.gateway]

- With nmtui

        nmtui

- With ip

        ip address [change|add|replace] dev ens36 10.10.10.72/24 broadcast 10.10.10.255

    or

        ip address [change|add|replace] dev ens36 192.168.19.254/24 broadcast 192.168.19.254 peer 192.168.19.72

- With ifconfig

        ifconfig ens36 192.168.19.254 netmask 255.255.255.0 broadcast 192.168.19.255



# About routing command in Linux

- Use `ip route` and `route`


- With route:

        route [add|del|mod]  [default|address] nestmask gw [default|address]  dev ifname

    Example:

        route add 10.10.10.0/24 gw default dev ens33


- With ip route:

        ip route add 10.10.10.0/24 via default dev ens33

- With nmcli:

        nmcli connection modify ens36 +ipv4.routes 10.10.10.0/24 ipv4.gateway 172.16.69.1