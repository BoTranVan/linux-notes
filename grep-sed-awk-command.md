# grep-sed-awk-command

1. `grep command`:

- Using to print line(s) which matched with a pattern
- Can read stdin from a pipe
- Can using a patter:

    + `+ one or more times`
    + `* zero or more times`
    + `? zero or one`

- See more with `regexp` in google.

- Using `-i option` to ignore sensitive case. Example:

        grep -in `ssh` filename.sh

    with

        cat filename.sh

        ssh
        SSH
        sSh

    We see:

        ssh
        SSH
        sSh

- Using `-v option` to print all lines not matched. Example:

        grep -v `ssh` filename.sh

    will be printed:

        SSh
        sSh

- Using `-c option` to print a count of matching.
- Using `-n option` to print lines number have matching.
- Using `-w option` to print lines containing matches that form whole words.

- Using options:

    - `e - go to the end of the current word.`
    - `E - go to the end of the current WORD.`
    - `b - go to the previous (before) word.`
    - `B - go to the previous (before) WORD.`
    - `w - go to the next word.`
    - `W - go to the next WORD.`

- Using `-l option` to show filename have matching.
- Using `-o option` to show only the matched string

2. `sed command`: Find and Replace text inside a File using RegEx
- Syntax:

        #sed '/ADDRESS/s/REGEXP/REPLACEMENT/FLAGS' filename

    or

        #sed '/PATTERN/s/REGEXP/REPLACEMENT/FLAGS' filename

with

- s is substitute command
- / is a delimiter
- REGEXP is regular expression to match
- REPLACEMENT is a value to replace
- FLAGS can be any of the following:
    - g Replace all the instance of REGEXP with REPLACEMENT
    - n Could be any number,replace nth instance of the REGEXP with REPLACEMENT.
    - p If substitution was made, then prints the new pattern space.
    - i match REGEXP in a case-insensitive manner.
    - w file If substitution was made, write out the result to the given file.
    - We can use different delimiters ( one of @ % ; : ) instead of /

Following [thegeekstuff](https://www.thegeekstuff.com/2009/09/unix-sed-tutorial-replace-text-inside-a-file-using-substitute-command/)

- Using `-i option` to edit files.
- We have `=` is a command to get a line number of a file. So:

        sed '/./=' filename.sh

    to insert lines number to each line

- Append line using `sed command`:

        sed 'ADDRESS a\content' filename

    or

        sed '/PATTERN/ a\content' filename

    note: ADDRESS is line of number.

- Like we have:

        - `a\` using to append content
        - `i\` using to insert content
        - `c\` using to replace content

3. `awk command`.


- `awk programs` are composed of pairs of the form:

        pattern {action}

    - action maybe is a sequence of statements, commands (run with system function), user defined-functions, [see more](https://www.tutorialspoint.com/awk/awk_overview.htm)

- Struct of awk program

        BEGIN {action}

        shell_functions\
        {actions}

        END {action}


- A column is with $n. Example:

        -rw-------  1 ministry ministry  1250 Th05  2 18:23 .xsession-errors.old

    we have:

        - `$0`: `-rw-------  1 ministry ministry  1250 Th05  2 18:23 .xsession-errors.old`
        - `$1`: `-rw-------`
        - `$9`: `.xsession-errors.old`
