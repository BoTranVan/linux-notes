# Tokens

Trong Openstack có 3 loại tokens cơ bản là:
1. UUID
2. PKI/ PKIz
3. Fernet


### UUID

- Là một chuỗi ngẫu nhiên 32 ký tự được. dễ dàng sử  dụng
- Cần được duy trì trong database của keystone quản lý.
- Không đem nhiều thông tin cho quá trình xác thực. Mỗi khi các dịch vụ cần xác thực sử dụng UUID token thì đều phải gửi lại cho keystone để kiếm tra. Sau đó, keystone cần phần gửi trở lại dịch vụ một lần nữa. Có thể gây ra quá tải về hiệu năng


### PKI/ PKIz

- Khắc phục nhược điểm của UUID, cần được lưu trữ trong database của keystone quản lý.
- Token là toàn bộ các thông tin được phản hồi từ keystone vao gồm, domain, project,  ... => kích thuước khá là lớn so với một HTTP Header (~8K)
- Các services có thể cache lại token và sử dụng cho tới khi token hết hạn hoặc bị  revoked. ( nếu như token đã được ủy quyền sử dụng cho một dịch vụ cụ thể )
- Vì kích thước lớn, lên thường xuyên phải flush database.

### Fernet

- Chỉ khoảng 255 ký tự nhưng lại đem đầy đủ thông tin cho quá trình xác thực (nhỏ hơn so với PKI/ PKIz)
- Vẫn cần Keystone xác thực token giống như sử dụng UUID, nhưng không cần phải lưu trữ vào trong database.
- Nhược điểm: Các khóa đối xứng sinh ra token cần phải được distributed và rotated.