# head tail less cut wc


1. `head command`: Default print first 10 lines of file to stdout. If have more than a file, command precede each with a header giving the filename.

- Example:

        head /var/log/*.conf

    or

        head /var/log/auth.log


- Using `-n option` to print n lines instead of the first 10.


2. `tail command`: Default print last 10 lines of file to stdout. If have more than a file, command precede each with a header giving the filename.

- Example:

        head /var/log/*.conf

    or

        head /var/log/auth.log


- Using `-n option` to print n lines instead of the first 10. And use -n +K to output lines starting with the K index.


3. `less command`: Using to view all and searching, ... like vi or more.


4. `cut command`: remove sections from each line of files

        tail /var/log/auth.log > filename.sh

- Using:
    + `-b option` to interact bytes
    + `-c option` to interact characters
    + `-f option` to interact fields


- Above options can use same arguments like:

    - N      N'th byte, character or field, counted from 1
    - N-     from N'th byte, character or field, to end of line
    - N-M    from N'th to M'th (included) byte, character or field
    - -M     from first to M'th (included) byte, character or field


5. `wc command`: Using to count a line show in terminal or print newline, word, and byte counts for each file
- Using:
    + `-c` to counter bytes
    + `-m` to counter characters
    + `-l` to counter lines
    + `-w` to counter words

- Example:

        cat filename.sh | wc -l