Shell History

- Using `history` to history command inputed. Like

          477  cat filename.sh s 2>| tee
          478  cat tee
          479  ls
          480  cat err
          481  cat err | read b
          482  b
          483  echo $b
          484  read b
          485  b
          486  echo $b
          487  ls
          488  cat err
          489  cat err | tee xxx
          490  ls
          491  rm -rf tee xxx
          492  ls
          493  cat err | tee xxx
          494  ls
          495  cat xxx
          496  ls
          497  history

    with form: `n   content_of_command`


- Using !n to re-run command with n index, example:

        !495

- Using !! to re-run command previous


- Using `history -c` to delete all history command